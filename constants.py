ENVIRONMENTS = {
    'prop': 'proposition',
    'satz': 'theorem',
    'kor': 'corollary',
    'bsp': 'example',
    'bem': 'remark',
    'thm': 'theorem',
    'cor': 'corollary',
}

ALPHABETS = {
    'b': 'mathbb',
    'c': 'mathcal',
    'f': 'mathfrak',
    '': 'mathbb',
}

DEPRECATED = {
    r'\eps': r'\varepsilon',
    r'\inv': r'^{-1}',
    r'\se': r'\subseteq',
    r'\sm': r'\setminus',
    r'\st': r'^{\ast}',
    r'\Tens': r'\tensor',
    r'\ann': r'\Ann',
    r'\Id': r'\id',
    r'\cat': r'\mathcat',
    r'\ob': r'\Ob',
}

UNICODE = {
    '∃': r'\exists',
    '∀': r'\forall',
    'ℕ': r'\mathbb{N}',
    'ℤ': r'\mathbb{Z}',
    'Λ': r'\Lambda',
    '⇔': r'\iff',
    '⇒': r'\implies',
    '⇐': r'\impliedby'
}

UTF8_REPLACEMENTS = {
        r'\"o': 'ö',
        r'\"a': 'ä',
        r'\"u': 'ü',
        r'\"O': 'Ö',
        r'\"A': 'Ä',
        r'\"U': 'Ü',
        r'{\ss}': 'ß',
        r'{ß}': 'ß',
}

TEX_SCRIPTSTYLES = {
        r'\em' : r'\emph'
}
