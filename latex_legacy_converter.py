from constants import *
import re
from pathlib import Path

def migrate_old_utf8_escape(line: str):
    for old, new in UTF8_REPLACEMENTS.items():
        while True:
            old = old.replace('\\', r'\\')
            m = re.search(old, line)
            if m is None:
                break
            else:
                line = line[:m.start()] + new + line[m.end():]
    return line

def migrate_tex_script_styles(line: str):
    for old, new in TEX_SCRIPTSTYLES.items():
        while True:
            old = old.replace('\\', r'\\')
            m = re.search(r'{' + old + r' *(.*?)}', line)
            if m is None:
                break
            else:
                line = line[:m.start()] + new + '{' + m.groups()[0] + '}' + line[m.end():]
    return line

def migrate_old_quotes(line: str):
    while True:
        m = re.search(r',,(.*?)``', line)
        if m is None:
            break
        else:
            line = line[:m.start()] + r'\enquote{' + m.groups()[0] + r'}' + line[m.end():]
            print(line)
    return line


def _migrate_lazy_math_alphabet(line: str, letters: str, short: str, alph: str):
    for letter in letters:
        while True:
            if r'\fS' in line:
                pass
            m = re.search(r'\\%s%s(?![a-zA-Z@])' % (short, letter), line)
            if m is None:
                break
            else:
                line = line[:m.start()] + r'\%s{%s}' % (alph, letter) + line[m.end():]
    return line


def migrate_lazy_math_alphabets(line: str) -> str:
    for short, alph in ALPHABETS.items():
        if short == '':
            line = _migrate_lazy_math_alphabet(line, 'NZQRCFK', '', alph)
        else:
            line = _migrate_lazy_math_alphabet(line, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', short, alph)
        if short == 'f':
            line = _migrate_lazy_math_alphabet(line, 'klmspqabc', short, alph)
    return line


def migrate_deprecated(line: str) -> str:
    if '∃' in line:
        foo = 2
    for old, new in DEPRECATED.items():
        while True:
            old = old.replace('\\', r'\\')
            m = re.search(old + '(?![a-zA-Z@])', line)
            if m is None:
                break
            else:
                line = line[:m.start()] + new + line[m.end():]
    return line


def replace_unicode(line: str) -> str:
    for old, new in UNICODE.items():
        line = line.replace(old, r'\ensuremath{%s}' % new)
    return line


def migrate_environments(line: str) -> str:
    for old_env, new_env in ENVIRONMENTS.items():
        line = line.replace(r'\begin{%s}' % old_env, r'\begin{%s}' % new_env)
        line = line.replace(r'\end{%s}' % old_env, r'\end{%s}' % new_env)
    return line


def optimize_line(line: str) -> str:
    line = migrate_environments(line)
    line = migrate_lazy_math_alphabets(line)
    line = migrate_deprecated(line)
    line = replace_unicode(line)
    line = migrate_old_utf8_escape(line)
    line = migrate_old_quotes(line)
    line = migrate_tex_script_styles(line)
    return line


def convert(path: Path):
    with path.open('r') as f:
        lines = f.readlines()
    newlines = []
    for line in lines:
        newlines.append(optimize_line(line))

    with path.open('w') as f:
        path.write_text(''.join(newlines))


if __name__ == "__main__":
    here = Path('.').resolve()
    files = here.rglob('*.tex')
    for file in files:
        convert(Path(file))
