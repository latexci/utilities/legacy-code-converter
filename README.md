# Legacy Code Converter
A simple, hacky python script that does replacement on LaTeX documents to bring them into a more consistent/modern format.

**Caution**: Running this in your directly will modify files in-place.
It is recommended to use a version control system to track the modifications and have a look at them after running this.

## Configuration:
There is no configuration file. Instead, you can edit the `constants.py` file,
where lots of replacements are defined.
Also, in case you want to deactivate some placements, edit the `optimize_line` method in `latex_legacy_converter.py`.
